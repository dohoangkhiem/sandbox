from twitter import api
import json

user = api.me()
print 'Authenticated user: name = {}, screen name = {}, followers count = {}, friends count = {}'.format(
    user.name, user.screen_name, user.followers_count, user.friends_count)

print 'Rate Limit'
print json.dumps(api.rate_limit_status(), indent=4, separators=(',', ': '))
