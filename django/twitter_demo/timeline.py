from twitter import api
import json

public_tweets = api.home_timeline()
for tweet in public_tweets:
    print u'{} says: {}, at {}, retweet = {}, favorite = {}'.format(tweet.user.name, tweet.text, str(tweet.created_at),
                                                                  tweet.retweet_count, tweet.favorite_count)
