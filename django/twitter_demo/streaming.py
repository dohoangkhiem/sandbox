from twitter import api, auth
import json
from tweepy import OAuthHandler, Stream
from tweepy.streaming import StreamListener


class StdOutListener(StreamListener):
    """ A listener handles tweets are the received from the stream.
    This is a basic listener that just prints received tweets to stdout.
    """
    def on_data(self, data):
        print(data)
        return True

    def on_error(self, status):
        print(status)

listener = StdOutListener()
stream = Stream(auth, listener)
stream.filter(track=['football'])

