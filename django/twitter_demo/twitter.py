import settings
import logging
import json
import tweepy

consumer_key = settings.twitter_consumer_key
consumer_secret = settings.twitter_consumer_secret
access_token = settings.twitter_access_token
access_token_secret = settings.twitter_access_token_secret

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

api = tweepy.API(auth)
