from django.contrib import admin
from polls.models import Poll, Choice

# choices will be added/updated/deleted inline from Poll admin when creating/editing a poll
class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 3
    
# admin model of Polls
class PollAdmin(admin.ModelAdmin):
    list_display = ('question', 'pub_date', 'was_published_recently')
    list_filter = ['pub_date']
    search_fields = ['question']
    
    fieldsets = [
        (None, { 'fields': ['question'] }),
        ('Date information', { 'fields': ['pub_date'], 'classes': ['collapse'] }),
    ]
    
    inlines = [ChoiceInline]


# Register your models here.
# register PollAdmin for Poll objects
admin.site.register(Poll, PollAdmin)
#admin.site.register(Choice)