from django.contrib import admin
from models import Student

class StudentAdmin(admin.ModelAdmin):
    list_filter = ['name', 'age']
    list_display = ['name', 'age']
    search_fields = ['name']


# Register your models here.
admin.site.register(Student, StudentAdmin)