from django.shortcuts import render
from django.views import generic
from models import Student


# Create your views here.
class StudentListView(generic.ListView):
    template_name = 'test1/student_list.html'
    context_object_name = 'students_list'

    # return list of all Student objects
    def get_queryset(self):
        return Student.objects.all()