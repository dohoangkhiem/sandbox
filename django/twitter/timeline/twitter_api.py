"""
Entry point to access Twitter APIs
"""

import tweepy
from models import Tweet, TwitterUser
import settings
import logging
import json

logger = logging.getLogger(__name__)

consumer_key = settings.twitter_consumer_key
consumer_secret = settings.twitter_consumer_secret
access_token = settings.twitter_access_token
access_token_secret = settings.twitter_access_token_secret

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

api = tweepy.API(auth)

logger.info('Authenticated user: ' + api.me().name)

def my_timeline():
    public_tweets = api.home_timeline()
    tweet_list = []
    for tweet in public_tweets:
        twitter_user = TwitterUser(user_id=tweet.user.id, username=tweet.user.name, description=tweet.user.description)
        tweet_obj = Tweet(status=tweet.text, tid=tweet.id_str, author=twitter_user, created_date_str=tweet.created_at,
                          retweet_count=tweet.retweet_count, favourite_count=tweet.favorite_count)
        tweet_list.append(tweet_obj)

    return tweet_list


def stream():
    pass




