from django.contrib import admin
from models import TwitterUser, Tweet, TweetStream


# Register your models here.


class TweetAdmin(admin.ModelAdmin):
    list_display = ['tid', 'status', 'author', 'created_date_str', 'retweet_count', 'favourite_count']
    search_fields = ['author']
    list_filter = ['author']
    change_list_template = 'admin/tweet_change_list.html'

class TwitterUserAdmin(admin.ModelAdmin):
    list_display = ['user_id', 'username', 'is_following', 'is_follower', 'description']
    search_fields = ['username']
    list_filter = ['is_following', 'is_follower']

class TweetStreamAdmin(admin.ModelAdmin):
    list_display = ['tid', 'status', 'author', 'created_date_str', 'retweet_count', 'favourite_count']
    search_fields = ['author']
    list_filter = ['author']
    change_list_template = 'admin/tweet_change_list.html'

admin.site.register(Tweet, TweetAdmin)
admin.site.register(TwitterUser, TwitterUserAdmin)
admin.site.register(TweetStream, TweetStreamAdmin)