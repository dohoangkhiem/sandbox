from django.test import TestCase
import twitter_api

# Create your tests here.


class TimelineTest(TestCase):
    def test_get_my_timeline(self):
        tweets = twitter_api.my_timeline()
        print 'Total = {}'.format(len(tweets))
        print tweets
        self.assertTrue(len(tweets) > 0)
        for tweet in tweets:
            twitter_user = tweet.author
            twitter_user.save()
            tweet.author = twitter_user
            tweet.save()
