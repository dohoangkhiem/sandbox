from django.db import models

# Create your models here


class TwitterUser(models.Model):
    user_id = models.CharField('User ID', max_length=40)
    username = models.CharField('User Name', max_length=100)
    is_following = models.BooleanField('Is following', default=False)
    is_follower = models.BooleanField('Is follower', default=False)
    description = models.TextField(null=True)

    def __unicode__(self):
        return self.username


class Tweet(models.Model):
    tid = models.CharField('Tweet ID', max_length=40)
    status = models.CharField('Status', max_length=140)
    author = models.ForeignKey(TwitterUser, verbose_name='Author')
    created_date = models.DateTimeField('Created Date', 'created date', null=True, blank=True)
    created_date_str = models.CharField(max_length=100)
    retweet_count = models.IntegerField()
    favourite_count = models.IntegerField()

    def __unicode__(self):
        return "Text = " + unicode(self.status)

class TweetStream(models.Model):
    tid = models.CharField('Tweet ID', max_length=40)
    status = models.CharField('Status', max_length=140)
    author = models.ForeignKey(TwitterUser, verbose_name='Author')
    created_date = models.DateTimeField('Created Date', 'created date', null=True, blank=True)
    created_date_str = models.CharField(max_length=100)
    retweet_count = models.IntegerField()
    favourite_count = models.IntegerField()

    def __unicode__(self):
        return "Text = " + unicode(self.status)