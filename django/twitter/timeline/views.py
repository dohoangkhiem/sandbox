from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.views import generic
from models import Tweet, TwitterUser
from django.contrib.auth.decorators import login_required
import twitter_api
import json

# Create your views here.


class HomeTimelineView(generic.ListView):
    model = Tweet
    template_name = 'tl/blog.html'
    context_object_name = 'tweets'
    paginate_by = 10
    def get_queryset(self):
        return Tweet.objects.order_by('-tid')


@login_required
def get_timeline(request):
    tweets = twitter_api.my_timeline()
    new_tweet_count = 0
    for tweet in tweets:
        try:
            Tweet.objects.get(tid=tweet.tid)
        except Tweet.DoesNotExist:
            user_existed = TwitterUser.objects.filter(user_id=tweet.author.user_id).exists()
            if not user_existed:
                tweet.author.save()
            twitter_user = TwitterUser.objects.get(user_id=tweet.author.user_id)
            tweet.author = twitter_user
            tweet.save()
            new_tweet_count += 1

    return HttpResponse(json.dumps({'count': new_tweet_count}), content_type="application/json")

