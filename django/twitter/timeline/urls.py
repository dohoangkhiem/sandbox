from django.conf.urls import patterns, url
import views

urlpatterns = patterns(
    '',
    url(r'^$', views.HomeTimelineView.as_view(), name='home'),
    url(r'^timeline/$', views.HomeTimelineView.as_view(), name='home_timeline'),
    url(r'^get_timeline/$', views.get_timeline, name='get_timeline'),
)