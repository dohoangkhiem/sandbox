from django.contrib import admin
from models import Post, Comment

# Register your models here.

class CommentAdmin(admin.ModelAdmin):
    list_display = ['author', 'message', 'created']
    search_fields = ['author', 'post']

class CommentInline(admin.TabularInline):
    model = Comment
    extra = 3

class PostAdmin(admin.ModelAdmin):
    search_fields = ['title']
    inlines = [CommentInline]

admin.site.register(Post, PostAdmin)
admin.site.register(Comment, CommentAdmin)