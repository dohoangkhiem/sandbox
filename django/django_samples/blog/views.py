from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from models import Post, Comment, CommentForm
from django.views import generic
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout

# Create your views here.


class MainView(generic.ListView):
    """Main blog post listing"""

    model = Post
    template_name = 'blog/index.html'
    context_object_name = 'posts'

    paginate_by = 2

    def get_queryset(self):
        # get all posts order by create date, from newest to oldest
        posts = Post.objects.all().order_by("-created")
        return posts

        # paging posts, 2 posts in each page
        #paginator = Paginator(posts, 2)

        #try:
        #    page = int(request.GET.get('page', '1'))
        #except ValueError:
        #    page = 1

        #try:
        #    posts = paginator.page(page)
        #except (InvalidPage, EmptyPage):
        #    posts = paginator.page(paginator.num_pages)

    # return render(request, 'blog/index.html', {
    #    'posts': posts
    # })


#class PostView(generic.DetailView):
#    model = Post
#    context_object_name = 'post'
#    template_name = 'blog/post.html'

def post_view(request, pk):
    post = get_object_or_404(Post, pk=pk)
    return render(request, 'blog/post.html', {
        'post': post,
        'cm_form': CommentForm()
    })


@login_required
def post_comment(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    cm_author = request.POST['comment-author']
    cm_message = request.POST['comment-message']
    if not cm_author or not cm_message:
        return render(request, 'blog/post.html', {
            'post': post,
        })

    comment = Comment.objects.create(author=cm_author, message=cm_message, post=post)
    return HttpResponseRedirect(reverse('blog:post', args=(post_id,)))




def blog_login(request):
    username = request.POST['username']
    password = request.POST['password']

    redirect_to = request.REQUEST.get('return_url', '')

    user = authenticate(username=username, password=password)
    if user is not None:
        #if user.is_active:
        login(request, user)
        if redirect_to:
            return HttpResponseRedirect(redirect_to)
        else:
            return HttpResponseRedirect(reverse('blog:main'))
        #else:
    else:
        return HttpResponseRedirect(reverse('blog:main'))


def blog_logout(request):
    redirect_to = request.REQUEST.get('return_url', reverse('blog:main'))
    logout(request)
    return HttpResponseRedirect(redirect_to)



