from django.db import models

# Create your models here.
from django.forms import ModelForm


class Post(models.Model):
    title = models.CharField(max_length=80)
    body = models.TextField()
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.title


class Comment(models.Model):
    author = models.CharField(max_length=80)
    message = models.CharField(max_length=1000)
    created = models.DateTimeField(auto_now_add=True)
    post = models.ForeignKey(Post)

    def __unicode__(self):
        return self.message


# need to find a way to specify field names in the form
class CommentForm(ModelForm):
    class Meta:
        model = Comment
        exclude = ['post']


