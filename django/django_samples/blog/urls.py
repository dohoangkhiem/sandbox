from django.conf.urls import patterns, url
from blog import views
import django.contrib.auth.views

urlpatterns = patterns(
    '',
    url(r'^$', views.MainView.as_view(), name='main'),
    url(r'^post/(?P<pk>\d+)$', views.post_view, name='post'),
    url(r'^post/(?P<post_id>\d+)/post_comment$', views.post_comment, name='post-comment'),
    url(r'^auth/login$', views.blog_login, name='login'),
    url(r'^auth/logout$', views.blog_logout, name='logout'),
    url(r'^auth/login$', django.contrib.auth.views.login, name='django.contrib.auth.views.login'),
    url(r'^auth/logout$', django.contrib.auth.views.logout, name='django.contrib.auth.views.logout'),
)