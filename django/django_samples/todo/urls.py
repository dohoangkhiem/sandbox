from django.conf.urls import patterns, url
from todo import views

urlpatterns = patterns(
    '',
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^create/$', views.create_task, name='create'),
)