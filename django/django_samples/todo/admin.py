from django.contrib import admin
from models import Item, DateTime


class ItemAdmin(admin.ModelAdmin):
    list_display = ['name', 'created', 'priority', 'difficulty', 'done']
    search_fields = ['name']
    list_filter = ['done']


class ItemInline(admin.TabularInline):
    model = Item


class DateAdmin(admin.ModelAdmin):
    list_display = ['datetime']
    inlines = [ItemInline]


# Register your models here
admin.site.register(Item, ItemAdmin)
admin.site.register(DateTime, DateAdmin)