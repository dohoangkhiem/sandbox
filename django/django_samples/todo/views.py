from django.shortcuts import render
from django.views import generic
from models import Item, DateTime
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from datetime import datetime

# Create your views here.


class IndexView(generic.ListView):
    template_name = 'todo/index.html'
    context_object_name = 'todo_list'

    def get_queryset(self):
        return Item.objects.all()


def create_task(request):
    task_text = request.POST['task']
    print(task_text)
    if not task_text:
        return render(request, 'todo/index.html', {
            'error_message': "You didn't select a choice.",
            'todo_list': Item.objects.all()
        })

    dtime = DateTime.objects.create()
    task = Item.objects.create(name=task_text, created=dtime)
    return HttpResponseRedirect(reverse('todo:index'))