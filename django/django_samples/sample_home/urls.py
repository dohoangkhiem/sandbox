from django.conf.urls import patterns, include, url

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^$', 'home.views.index'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^todo/', include('todo.urls', namespace='todo')),
    url(r'^blog/', include('blog.urls', namespace='blog')),
    url(r'^rest/', include('rest_demo.urls', namespace='rest_demo')),
)
